### README

These are sample config files for a local gem server running nginx + geminabox.<br/>
The geminabox Ruby server can be run with puma like this:
```
puma -C puma.rb > puma.log & disown
```